import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:vorazrtr/data/services/odoo_api.dart';
import 'package:vorazrtr/pages/home_page.dart';
import 'package:vorazrtr/pages/login_page.dart';
import 'package:vorazrtr/utils/nav.dart';

import 'base.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends Base<SplashPage> {
  @override
  void initState() {
    super.initState();

    Future fa = getOdooInstance();

    Future.delayed(Duration(seconds: 4)).then((_) {
      Future.wait([fa]).then((List values) {
        return isLoggedIn()
            ? push(context, HomePage(), replace: true)
            : push(context, LoginPage(), replace: true);
      });
    });
  }

  Future<Odoo> odooInstance() async {
    Odoo odoo = await getOdooInstance();
    return odoo;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: FlareActor("assets/animations/loading.flr",
            animation: "splash")
        ),
      ),
    );
  }
}
