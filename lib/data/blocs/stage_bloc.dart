import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/crops_api.dart';
import 'package:vorazrtr/data/apis/stage_api.dart';
import 'package:vorazrtr/data/daos/crops_dao.dart';
import 'package:vorazrtr/data/daos/stage_dao.dart';
import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/data/models/stage.dart';
import 'package:vorazrtr/utils/simple_bloc.dart';

class StageBloc extends SimpleBloc<List<Stage>> {
  Base base;

  Future<List<Stage>> fetch(Base b) async {
    try {
      base = b;

      List<Stage> stages = await StageApi.getStage(base);

      final dao = StageDao();

      stages.forEach((p) => dao.save(p));

      add(stages);

      return stages;
    } catch (e) {
      addError(e);
      return [];
    }
  }
}