import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/state_api.dart';
import 'package:vorazrtr/data/daos/state_dao.dart';
import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/utils/simple_bloc.dart';

class StateBloc extends SimpleBloc<List<CountryState>> {
  Base base;

  Future<List<CountryState>> fetch(Base b) async {
    try {
      base = b;

      List<CountryState> states = await StateApi.getStates(base);

      final dao = StateDao();

      states.forEach((p) => dao.save(p));

      add(states);

      return states;
    } catch (e) {
      addError(e);
      return [];
    }
  }
}