import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/utils/simple_bloc.dart';

class RtrBloc extends SimpleBloc<List<Rtr>> {
  Base base;

  Future<Rtr> fetchOne(Base b, int id) async {
    try {
      base = b;

      Rtr rtr = await RtrApi.getRtr(base, id);

      return rtr;
    } catch (e) {
      addError(e);
      return Rtr();
    }
  }

  Future<List<Rtr>> fetchList(Base b) async {
    try {
      base = b;

      List<Rtr> rtrs = await RtrApi.getRtrs(base);

//      final dao = RtrDao();
//
//      rtrs.forEach((p) => dao.save(p));

      add(rtrs);

      return rtrs;
    } catch (e) {
      addError(e);
      return [];
    }
  }

  Future<List<Rtr>> fetchUpdate(Base b) async {
    try {
      base = b;

      List<Rtr> rtrs = await RtrApi.getRtrs(base);

      final dao = RtrDao();

      rtrs.forEach((p) => dao.save(p));

      add(rtrs);

      return rtrs;
    } catch (e) {
      addError(e);
      return [];
    }
  }

}
