import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/crops_api.dart';
import 'package:vorazrtr/data/daos/crops_dao.dart';
import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/utils/simple_bloc.dart';

class CropsBloc extends SimpleBloc<List<Crops>> {
  Base base;

  Future<List<Crops>> fetch(Base b) async {
    try {
      base = b;

      List<Crops> crops = await CropsApi.getCrops(base);

      final dao = CropsDao();

      crops.forEach((p) => dao.save(p));

      add(crops);

      return crops;
    } catch (e) {
      addError(e);
      return [];
    }
  }
}