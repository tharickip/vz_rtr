import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/utils/sql/base_dao.dart';

class CityDao extends BaseDAO<City> {
  @override
  String get tableName => "city";

  @override
  City fromMap(Map<String, dynamic> map) {
    return City.fromMap(map);
  }

  Future<List<City>> findCityByState(int stateId) async {
    try {
      return await query('select * from city where state_id = ?', [stateId]);
    } catch (e) {
      print("Erro de SQL: $e");
      return [];
    }
  }
}
