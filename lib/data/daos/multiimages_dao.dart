import 'package:vorazrtr/data/models/multi_image.dart';
import 'package:vorazrtr/utils/sql/base_dao.dart';

class MultiImageDao extends BaseDAO<MultiImage> {
  @override
  String get tableName => "multi_images";

  @override
  MultiImage fromMap(Map<String, dynamic> map) {
    return MultiImage.fromMap(map);
  }

  Future<List<MultiImage>> findMultiImages(int id) async {
    try {
      return await query('select * from $tableName where visit_id = ?', [id]);
    } catch (e) {
      print("Erro de SQL: $e");
      return [];
    }
  }
}