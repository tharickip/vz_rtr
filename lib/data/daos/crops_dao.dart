import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/utils/sql/base_dao.dart';

class CropsDao extends BaseDAO<Crops> {
  @override
  String get tableName => "crops";

  @override
  Crops fromMap(Map<String, dynamic> map) {
    return Crops.fromMap(map);
  }

  Future<List<Crops>> findCrops() async {
    try {
      return findAll();
    } catch (e) {
      print("Erro de SQL: $e");
      return [];
    }
  }
}
