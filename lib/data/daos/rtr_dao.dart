import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/utils/sql/base_dao.dart';

class RtrDao extends BaseDAO<Rtr>{
  @override
  String get tableName => "rtr";

  @override
  Rtr fromMap(Map<String, dynamic> map) {
    return Rtr.fromMap(map);
  }

  Future<List<Rtr>> findRtrs() async {
    try {
      return findAll();
    } catch (e) {
      print("Erro de SQL: $e");
      return [];
    }
  }

  Future<List<Rtr>> findVisits(int id) async {
    try {
      return await query('select * from rtr where rtr_id = ?', [id]);
    } catch (e) {
      print("Erro de SQL: $e");
      return [];
    }
  }

}