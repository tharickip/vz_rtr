import 'package:vorazrtr/data/models/stage.dart';
import 'package:vorazrtr/utils/sql/base_dao.dart';

class StageDao extends BaseDAO<Stage> {
  @override
  String get tableName => "stage";

  @override
  Stage fromMap(Map<String, dynamic> map) {
    return Stage.fromMap(map);
  }

  Future<List<Stage>> findStage() async {
    try {
      return findAll();
    } catch (e) {
      print("Erro de SQL: $e");
      return [];
    }
  }
}
