import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/data/models/stage.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/utils/strings.dart';

class StageApi {
  static Future<List<Stage>> getStage(Base base) async {
    List _stage = new List<Stage>();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo
              .searchRead(Strings.vz_plant_stage, [], ['id', 'name']).then(
                (OdooResponse res) {
              for (var i in res.getRecords()) {
                _stage.add(
                  new Stage(
                    id: i["id"],
                    name: i["name"],
//                    crops: i["crops_id"][0],
                  ),
                );
              }
            },
          );
        } else {
//          base.showSnackBar(Strings.internetMessage);
//          List<Partner> lsPartner = await PartnerDao().findPartners();
//          _partners = lsPartner;
        }
      });
    });

    return _stage;
  }
}