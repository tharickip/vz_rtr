import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/blocs/campaign_bloc.dart';
import 'package:vorazrtr/data/blocs/city_bloc.dart';
import 'package:vorazrtr/data/blocs/crops_bloc.dart';
import 'package:vorazrtr/data/blocs/partner_bloc.dart';
import 'package:vorazrtr/data/blocs/stage_bloc.dart';
import 'package:vorazrtr/data/blocs/state_bloc.dart';
import 'package:vorazrtr/data/daos/campaign_dao.dart';
import 'package:vorazrtr/data/daos/city_dao.dart';
import 'package:vorazrtr/data/daos/crops_dao.dart';
import 'package:vorazrtr/data/daos/partner_dao.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/daos/stage_dao.dart';
import 'package:vorazrtr/data/daos/state_dao.dart';
import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/data/models/multi_image.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/models/stage.dart';
import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/utils/strings.dart';

class RtrApi {
  static Future<int> saveRtr(Base base, Map values) async {
    int status = 0;
    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo.create(Strings.vz_rtr, values).then(
            (OdooResponse res) async {
              //tratar resposta
              if (res.getStatusCode() == 200) {
                status = res.getResult();
              }
            },
          );
        }
      });
    });
    return status;
  }

  static Future<OdooResponse> deleteRtr(Base base, int id) async {
    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          List<int> ids = [];
          ids.add(id);
          await odoo.unlink(Strings.vz_rtr, ids).then(
            (OdooResponse res) async {
              //tratar resposta
              return res;
            },
          );
        }
      });
    });
  }

  static Future<Rtr> getRtr(Base base, int id) async {
    Rtr _rtr = Rtr();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo.searchRead(Strings.vz_rtr, [
            ['id', '=', id]
          ], [
            'id',
            'name',
            'report_name',
            'campaign_id',
            'partner_id',
            'state_id',
            'city_id',
            'user_id',
            'date_deadline',
            'crops_id',
            'plant_stage_id',
            'description',
            'final_considerations',
            'is_rtr',
            'message_ids',
            'count_rtr_childs',
            'multi_images',
            'rtr_id'
          ]).then(
            (OdooResponse res) async {
              for (var i in res.getRecords()) {
                try {
                  Partner partner;
                  Campaign campaign;
                  CountryState state;
                  City city;
                  Crops crops;
                  Stage stage;
                  if (i["partner_id"] != false) {
                    partner = await fullPartner(base, i);
                  }
                  if (i["campaign_id"] != false) {
                    campaign = await fullCampaign(base, i);
                  }
                  if (i["state_id"] != false) {
                    state = await fullState(base, i);
                  }
                  if (i["city_id"] != false) {
                    city = await fullCity(base, i);
                  }
                  if (i["crops_id"] != false) {
                    crops = await fullCrops(base, i);
                  }
                  if (i["plant_stage_id"] != false) {
                    stage = await fullStage(base, i);
                  }

                  _rtr = Rtr(
                      id: i["id"],
                      rtrId: i["rtr_id"] != false ? i["rtr_id"][0] : null,
                      name: i["name"],
                      reportName: i["report_name"],
                      userId: i["user_id"][0],
                      isRtr: i["is_rtr"].toString(),
                      visitDate: i["date_deadline"] != false
                          ? DateTime.parse(i["date_deadline"])
                          : null,
                      dateDeadline: i["date_deadline"] != false
                          ? i["date_deadline"]
                          : null,
                      description:
                          i["description"] != false ? i['description'] : "",
                      finalConsiderations: i["final_considerations"] != false
                          ? i["final_considerations"]
                          : "",
                      countRtrChilds: i["count_rtr_childs"],
                      messageIds: i["message_ids"],
                      multiImages:
                          i["multi_images"] != false ? i['multi_images'] : "",
                      farm: partner,
                      campaign: campaign,
                      state: state,
                      city: city,
                      stage: stage,
                      crops: crops);
                } catch (e) {
                  print("Erro de parse: $e");
                }
              }
            },
          );
        }
      });
    });

    return _rtr;
  }

  static Future<List<Rtr>> getRtrs(Base base) async {
    List _rtrList = new List<Rtr>();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo
              .searchRead(
                  Strings.vz_rtr,
                  [
                    ['type', '=', 'rtr'],
//                    ['is_rtr', '=', true],
                    ['user_id', '=', base.user.result.uid]
                  ],
                  [
                    'id',
                    'name',
                    'report_name',
                    'campaign_id',
                    'partner_id',
                    'state_id',
                    'city_id',
                    'user_id',
                    'date_deadline',
                    'crops_id',
                    'plant_stage_id',
                    'description',
                    'final_considerations',
                    'is_rtr',
                    'message_ids',
                    'count_rtr_childs',
                    'multi_images',
                    'rtr_id'
                  ],
                  limit: 50)
              .then(
            (OdooResponse res) async {
              for (var i in res.getRecords()) {
//                String session = base.getSession().split(",")[0].split(";")[0];
                try {
                  Partner partner;
                  Campaign campaign;
                  CountryState state;
                  City city;
                  Crops crops;
                  Stage stage;
                  if (i["partner_id"] != false) {
                    partner = await fullPartner(base, i);
                  }
                  if (i["campaign_id"] != false) {
                    campaign = await fullCampaign(base, i);
                  }
                  if (i["state_id"] != false) {
                    state = await fullState(base, i);
                  }
                  if (i["city_id"] != false) {
                    city = await fullCity(base, i);
                  }
                  if (i["crops_id"] != false) {
                    crops = await fullCrops(base, i);
                  }
                  if (i["plant_stage_id"] != false) {
                    stage = await fullStage(base, i);
                  }

                  _rtrList.add(
                    new Rtr(
                        id: i["id"],
                        rtrId: i["rtr_id"] != false ? i["rtr_id"][0] : null,
                        name: i["name"],
                        reportName: i["report_name"],
                        userId: i["user_id"][0],
                        isRtr: i["is_rtr"].toString(),
                        visitDate: i["date_deadline"] != false
                            ? DateTime.parse(i["date_deadline"])
                            : null,
                        dateDeadline: i["date_deadline"] != false
                            ? i["date_deadline"]
                            : null,
                        description:
                            i["description"] != false ? i['description'] : "",
                        finalConsiderations: i["final_considerations"] != false
                            ? i["final_considerations"]
                            : "",
                        countRtrChilds: i["count_rtr_childs"],
                        messageIds: i["message_ids"],
                        multiImages:
                            i["multi_images"] != false ? i['multi_images'] : "",
                        farm: partner,
                        campaign: campaign,
                        state: state,
                        city: city,
                        stage: stage,
                        crops: crops),
                  );
                } catch (e) {
                  print("Erro de parse: $e");
                }
              }
            },
          );
        } else {
          List<Rtr> lsRtr = await RtrDao().findRtrs();
          _rtrList = lsRtr;
        }
      });
    });

    return _rtrList;
  }

  static Future<Partner> fullPartner(Base base, i) async {
    PartnerDao pDao = PartnerDao();
    Partner p = await pDao.findById(i["partner_id"][0]);
    if (p == null) {
      final _bloc = PartnerBloc();
      await _bloc.fetch(base);
      return await pDao.findById(i["partner_id"][0]);
    } else {
      return p;
    }
  }

  static Future<Campaign> fullCampaign(Base base, i) async {
    CampaignDao cDao = CampaignDao();
    Campaign c = await cDao.findById(i["campaign_id"][0]);
    if (c == null) {
      final _bloc = CampaignBloc();
      await _bloc.fetch(base);
      return await cDao.findById(i["campaign_id"][0]);
    } else {
      return c;
    }
  }

  static Future<CountryState> fullState(Base base, i) async {
    StateDao sDao = StateDao();
    CountryState s = await sDao.findById(i["state_id"][0]);
    if (s == null) {
      final _bloc = StateBloc();
      await _bloc.fetch(base);
      return await sDao.findById(i["state_id"][0]);
    } else {
      return s;
    }
  }

  static Future<City> fullCity(Base base, i) async {
    CityDao cityDao = CityDao();
    City c = await cityDao.findById(i["city_id"][0]);
    if (c == null) {
      final _bloc = CityBloc();
      await _bloc.fetch(base);
      return await cityDao.findById(i["city_id"][0]);
    } else {
      return c;
    }
  }

  static Future<Crops> fullCrops(Base base, i) async {
    CropsDao cropsDao = CropsDao();
    Crops c = await cropsDao.findById(i["crops_id"][0]);
    if (c == null) {
      final _bloc = CropsBloc();
      await _bloc.fetch(base);
      return await cropsDao.findById(i["crops_id"][0]);
    } else {
      return c;
    }
  }

  static Future<Stage> fullStage(Base base, i) async {
    StageDao stageDao = StageDao();
    Stage stage = await stageDao.findById(i["plant_stage_id"][0]);
    if (stage == null) {
      final _bloc = StageBloc();
      await _bloc.fetch(base);
      return await stageDao.findById(i["plant_stage_id"][0]);
    } else {
      return stage;
    }
  }

  Future<dynamic> sendAnnotation(Base base, List<int> ids, String body) async {
    dynamic result;
    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo.writeAnnotation(Strings.vz_rtr, ids, body).then(
            (OdooResponse res) async {
              result = res.getResult();
            },
          );
        }
      });
    });
    return result;
  }

  Future<List<dynamic>> readAnnotations(Base base, List<dynamic> ids) async {
    List<dynamic> result = List();
    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo.readAnnotation(Strings.mail_message, ids).then(
            (OdooResponse res) async {
              result = res.getResult();
            },
          );
        }
      });
    });
    return result;
  }

  Future<List<MultiImage>> getMultiImage(Base base, List<dynamic> ids) async {
    List<MultiImage> images = List();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo.searchRead(Strings.multi_images, [
            ["id", "in", ids]
          ], [
            'id',
            'image',
            'title'
          ]).then(
            (OdooResponse res) async {
              for (var i in res.getRecords()) {
                images.add(MultiImage(
                    image: i['image'] != false ? i['image'] : "",
                    title: i['title'] != false ? i['title'] : ""));
              }
            },
          );
        }
      });
    });
    return images;
  }
}
