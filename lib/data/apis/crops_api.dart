import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/utils/strings.dart';

class CropsApi {
  static Future<List<Crops>> getCrops(Base base) async {
    List _crops = new List<Crops>();

    await base.getOdooInstance().then((odoo) async {
      await base.isConnected().then((isInternet) async {
        if (isInternet) {
          await odoo
              .searchRead(Strings.product_crops, [], ['id', 'name', 'active']).then(
                (OdooResponse res) {
              for (var i in res.getRecords()) {
                _crops.add(
                  new Crops(
                    id: i["id"],
                    name: i["name"],
                    active: i["active"],
                  ),
                );
              }
            },
          );
        } else {
//          base.showSnackBar(Strings.internetMessage);
//          List<Partner> lsPartner = await PartnerDao().findPartners();
//          _partners = lsPartner;
        }
      });
    });

    return _crops;
  }
}