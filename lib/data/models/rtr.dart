import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/data/models/multi_image.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/models/stage.dart';
import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/utils/sql/entity.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';


class Rtr extends Entity {
  int id;
  String name;
  String reportName;
  Partner farm;
  Campaign campaign;
  CountryState state;
  City city;
  String type;
  String isRtr;
  int userId;
  int rtrId;
  List<int> childIds;
  List<Rtr> visits;
  int countRtrChilds;

  //visits attributes
  DateTime visitDate;
  String dateDeadline;
  Crops crops;
  Stage stage;
  String description;
  String finalConsiderations;

  List<dynamic> multiImages;
  List<dynamic> messageIds;

  Rtr(
      {this.id,
      this.name,
      this.reportName,
      this.farm,
      this.campaign,
      this.state,
      this.city,
      this.visits,
      this.type,
      this.isRtr,
      this.userId,
      this.visitDate,
      this.dateDeadline,
      this.crops,
      this.stage,
      this.description,
      this.childIds,
      this.rtrId,
      this.finalConsiderations,
      this.countRtrChilds,
      this.multiImages,
      this.messageIds});

  Rtr.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    reportName = map['report_name'];
    farm = Partner(id: map['partner_id']);
    campaign = Campaign(id: map['campaign_id']);
    visitDate = map['date_deadline'] != null
        ? DateTime.parse(map['date_deadline'])
        : null;
    dateDeadline = map['date_deadline'];
    countRtrChilds = map['count_rtr_childs'];
    crops = Crops(id: map['crops_id']);
    stage = Stage(id: map['plant_stage_id']);
    city = City(id: map['city_id']);
    rtrId = map['rtr_id'];
    type = map['type'];
    description = map['description'];
    finalConsiderations = map['final_considerations'];
    isRtr = map['is_rtr'];
    userId = map['user_id'];
    multiImages = map['multi_images'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['report_name'] = this.reportName;
    data['partner_id'] = this.farm != null ? this.farm.id : null;
    data['campaign_id'] = this.campaign != null ? this.campaign.id : null;
    data['state_id'] = this.state != null ? this.state.id : null;
    data['city_id'] = this.city != null ? this.city.id : null;
    data['date_deadline'] =
        this.visitDate != null ? this.visitDate.toString() : null;
    data['crops_id'] = this.crops != null ? this.crops.id : null;
    data['plant_stage_id'] = this.stage != null ? this.stage.id : null;
    data['rtr_id'] = this.rtrId;
    data['type'] = this.type;
    data['description'] = this.description;
    data['final_considerations'] = this.finalConsiderations;
    data['is_rtr'] = this.isRtr;
    data['count_rtr_childs'] = this.countRtrChilds;
    data['multi_images'] = this.multiImages;
//    .length > 0 ? tripleMultiImage() : [];
    return data;
  }

  Map<String, dynamic> toMapToSend() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['report_name'] = this.reportName;
    data['partner_id'] = this.farm != null ? this.farm.id : null;
    data['campaign_id'] = this.campaign != null ? this.campaign.id : null;
    data['state_id'] = this.state != null ? this.state.id : null;
    data['city_id'] = this.city != null ? this.city.id : null;
    data['date_deadline'] =
        this.visitDate != null ? this.visitDate.toString() : null;
    data['crops_id'] = this.crops != null ? this.crops.id : null;
    data['plant_stage_id'] = this.stage != null ? this.stage.id : null;
    data['rtr_id'] = this.rtrId;
    data['type'] = this.type;
    data['description'] = this.description;
    data['final_considerations'] = this.finalConsiderations;
    data['is_rtr'] = this.isRtr;
    data['count_rtr_childs'] = this.countRtrChilds;
    data['multi_images'] =
        this.multiImages.length > 0 ? tripleMultiImage() : [];
    return data;
  }

  tripleMultiImage() {
    List images = [];
    for (MultiImage img in multiImages) {
      List details = [];
      final Map<String, dynamic> image = new Map<String, dynamic>();
      image['title'] = img.title;
      image['image'] = img.image;
      details.add(0);
      details.add(0);
      details.add(image);

      images.add(details);
    }

    return images;
  }
}
