import 'package:flutter/cupertino.dart';

class Choice {
  const Choice({this.option, this.title, this.icon});

  final String option;
  final String title;
  final IconData icon;
}