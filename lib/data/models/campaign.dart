import 'package:vorazrtr/utils/sql/entity.dart';

class Campaign extends Entity {
  int id;
  String name;

  Campaign({this.id, this.name});

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }

  Campaign.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
  }

  @override
  String toString() {
    // TODO: implement toString
    return name;
  }
}
