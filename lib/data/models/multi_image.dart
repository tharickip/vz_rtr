import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/utils/sql/entity.dart';

class MultiImage extends Entity {
  int id;
  String image;
  String title;
  int visitId;

  MultiImage({this.id, this.image, this.title, this.visitId});

  MultiImage.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    image = map['image'];
    title = map['title'];
    visitId = map['visit_id'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['title'] = this.title;
    data['visit_id'] = this.visitId;
    return data;
  }



  @override
  String toString() {
    String multiImage = "'image':"+image+",'title':"+title;

    return multiImage;
  }

  String getImage(){
    return this.image;
  }
}
