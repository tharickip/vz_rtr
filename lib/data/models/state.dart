import 'package:vorazrtr/utils/sql/entity.dart';

class CountryState extends Entity {
  int id;
  String name;
  String code;
  int countryId;

  CountryState({this.id, this.name, this.code, this.countryId});

  CountryState.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    code = map['code'];
    countryId = map['country_id'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['code'] = this.code;
    data['country_id'] = this.countryId;
    return data;
  }

  @override
  String toString() {
    // TODO: implement toString
    return name;
  }
}
