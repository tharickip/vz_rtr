import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/utils/sql/entity.dart';

class Stage extends Entity {
  int id;
  String name;
  Crops crops;

  Stage({this.id, this.name, this.crops});

  Stage.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
//    crops = map['crops_id'][0];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
//    data['crops_id'] = this.crops;
    return data;
  }

  @override
  String toString() {
    // TODO: implement toString
    return name;
  }
}
