import 'package:vorazrtr/utils/sql/entity.dart';

class Crops extends Entity {
  int id;
  String name;
  bool active;

  Crops({this.id, this.name, this.active});

  Crops.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    active = map['active'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['active'] = this.active;
    return data;
  }

  @override
  String toString() {
    // TODO: implement toString
    return name;
  }
}
