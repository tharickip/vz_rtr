import 'package:vorazrtr/utils/sql/entity.dart';

class Partner extends Entity {
  int id;
  String name;
  String imageUrl;
  String email;
  String phone;
  String address;

  Partner(
      {this.id,
      this.name,
      this.imageUrl,
      this.email,
      this.phone,
      this.address});

  Partner.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    email = map['email'];
    phone = map['phone'];
    address = map['address'];
    imageUrl = map['imageUrl'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['imageUrl'] = this.imageUrl;
    return data;
  }

  @override
  String toString() {
    // TODO: implement toString
    return name;
  }
}
