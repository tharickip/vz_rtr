import 'package:vorazrtr/utils/sql/entity.dart';

class City extends Entity {
  int id;
  String name;
  int stateId;

  City({this.id, this.name, this.stateId});

  City.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    stateId = map['state_id'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['state_id'] = this.stateId;
    return data;
  }

  @override
  String toString() {
    // TODO: implement toString
    return name;
  }
}
