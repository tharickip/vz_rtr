import 'package:vorazrtr/utils/sql/entity.dart';

class Annotation extends Entity {

  String annotation;
  String author;

  Annotation({this.annotation, this.author});

  Annotation.fromMap(Map<String, dynamic> map) {
    annotation = map['annotation'];
    author = map['author'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['annotation'] = this.annotation;
    data['author'] = this.author;
    return data;
  }

}
