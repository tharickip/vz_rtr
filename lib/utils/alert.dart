import 'package:flutter/material.dart';

showSnack(BuildContext context, String msg) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        textColor: Colors.green,
        label: "OK",
        onPressed: () {
          print("OK!");
        },
      ),
    ),
  );
}

alert(BuildContext context, String msg, {bool hideCancelBtn = false, Function callback}) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return WillPopScope(
        onWillPop: () async => false,
        child: AlertDialog(
          title: Text("Atenção!"),
          content: Text(msg),
          actions: <Widget>[
            !hideCancelBtn
                ? FlatButton(
                    child: Text("Cancelar"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                : null,
            FlatButton(
              child: Text("OK"),
              onPressed: () {
                Navigator.pop(context);
                if (callback != null) {
                  callback();
                }
              },
            )
          ],
        ),
      );
    },
  );
}
