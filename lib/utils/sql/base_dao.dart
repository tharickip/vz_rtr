import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:vorazrtr/utils/sql/db_helper.dart';
import 'package:vorazrtr/utils/sql/entity.dart';

// Data Access Object
abstract class BaseDAO<T extends Entity> {
  Future<Database> get db => DatabaseHelper.getInstance().db;

  String get tableName;

  T fromMap(Map<String, dynamic> map);

  Future<int> save(T entity) async {
    try {
      var dbClient = await db;
      var id = await dbClient.insert(tableName, entity.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
      print('id: $id');
      return id;
    } catch (e) {
      print("Insert error: (table $tableName) --> $e");
    }
  }

  Future<List<T>> query(String sql, [List<dynamic> arguments]) async {
    try {
      final dbClient = await db;

      final list = await dbClient.rawQuery(sql, arguments);

      return list.map<T>((json) => fromMap(json)).toList();
    } catch (e) {
      print("Query error: (tabela $tableName) --> $e");
    }
  }

  Future<List<T>> findAll() {
    return query('select * from $tableName');
  }

  Future<T> findById(int id) async {
    List<T> list = await query('select * from $tableName where id = ?', [id]);

    return list.length > 0 ? list.first : null;
  }

  Future<T> findByName(String name) async {
    List<T> list =
        await query('select * from $tableName where name = ?', [name]);

    return list.length > 0 ? list.first : null;
  }

  Future<bool> exists(int id) async {
    T c = await findById(id);
    var exists = c != null;
    return exists;
  }

  Future<int> count() async {
    final dbClient = await db;
    final list = await dbClient.rawQuery('select count(*) from $tableName');
    return Sqflite.firstIntValue(list);
  }

  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient
        .rawDelete('delete from $tableName where id = ?', [id]);
  }

  Future<int> deleteAll() async {
    var dbClient = await db;
    return await dbClient.rawDelete('delete from $tableName');
  }
}
