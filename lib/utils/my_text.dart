import 'package:flutter/material.dart';

Text text(
    String s, {
      double fontSize = 16,
      color = Colors.white,
      bold = false,
      maxLines = 2
    }) {
  return Text(
    s ?? "",
    overflow: TextOverflow.ellipsis,
    maxLines: maxLines,
    style: TextStyle(
      fontSize: fontSize,
      fontWeight: bold ? FontWeight.bold : FontWeight.normal,
    ),
  );
}