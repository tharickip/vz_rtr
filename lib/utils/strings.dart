class Strings {
  static const String app_title = "Voraz RTR";

  static const String internetMessage = "No Internet Connection!";
  static const String loginAgainMessage = "Setting Saved! Please Login!";
  static const String invalidUrlMessage = "Can't connect to the server! Please enter valid URL";
  static const String no_orders = "No Orders";

  // Route Name
  static const String route_home = "/home";
  static const String route_login = "/login";

  // Model name
  static const String res_partner = "res.partner";
  static const String res_users = "res.users";
  static const String res_country_state = "res.country.state";
  static const String res_city = "res.state.city";
  static const String product_crops = "product.crops";
  static const String vz_plant_stage = "vz.plant.stage";
  static const String sale_campaign = "sale.campaign";
  static const String vz_rtr = "vz.rtr";
  static const String multi_images = "multi.images";
  static const String mail_message = "mail.message";
}