import 'package:flutter/material.dart';

class AppTextfield extends StatelessWidget {
  String text;
  bool invisible;
  TextEditingController controller;
  TextInputAction textInputAction;
  FocusNode focusNode;
  FocusNode nextFocus;
  int maxLines;
  TextInputType inputType = TextInputType.text;
  TextCapitalization textCapitalization;
  final FormFieldValidator<String> validator;

  AppTextfield(this.text,
      {this.invisible,
      this.controller,
      this.maxLines,
      this.textInputAction,
      this.focusNode,
      this.nextFocus,
      this.validator,
      this.inputType,
      this.textCapitalization});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2),
      child: TextFormField(
        controller: controller,
        validator: _validRequired,
        textInputAction: textInputAction,
        focusNode: focusNode,
        maxLines: maxLines,
        onFieldSubmitted: (String text) {
          if (nextFocus != null) {
            FocusScope.of(context).requestFocus(nextFocus);
          }
        },
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.green, width: 0.0)),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          labelText: text,
          labelStyle: TextStyle(
            fontSize: 20,
            color: Colors.green,
          ),
        ),
        obscureText: invisible != null ? invisible : false,
        keyboardType: inputType,
      ),
    );
  }

  String _validRequired(String value) {
    if (value == null || value.isEmpty) {
      return 'Campo obrigatório';
    }

    return null;
  }
}
