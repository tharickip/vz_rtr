import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/utils/my_text.dart';

class PartnerPage extends StatefulWidget {

  Partner partner;

  PartnerPage(this.partner);

  @override
  _PartnerPageState createState() => _PartnerPageState();
}

class _PartnerPageState extends Base<PartnerPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.partner.name),
      ),
      body: _body(),
    );
  }

  _body() {
    print(widget.partner.imageUrl);
    return Container(
      padding: EdgeInsets.all(35),
      child: ListView(
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: widget.partner.imageUrl ?? ""),
          _partnerDetails(),
          Divider(),
//          _partnerVisits(),
        ],
      ),
    );
  }

  _partnerDetails() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Flexible(
          child: Column(
            children: <Widget>[
              text(widget.partner.name, fontSize: 20, bold: true),
              text("E-mail: ${widget.partner.email}", fontSize: 16),
              text("Fone: ${widget.partner.phone}", fontSize: 16),
              text("Endereço: ${widget.partner.address != null ? widget.partner.address : "N/A"}", fontSize: 16),
            ],
          )
        )
      ],
    );
  }
}