import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/pages/realtime_reports/visit_tabs_page.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/nav.dart';

class VisitsListview extends StatelessWidget {
  final List<Rtr> _rtrs;
  final Base base;

  VisitsListview(this._rtrs, {this.base});

  @override
  Widget build(BuildContext context) {
    initializeDateFormatting('pt_BR', null);

    return ListView.builder(
      itemCount: _rtrs.length,
      itemBuilder: (context, index) {
        return _rtrCard(context, index);
      },
    );
  }

  _rtrCard(BuildContext context, int i) {
    String dateString = _rtrs[i].visitDate != null
        ? DateFormat('dd/MM/yyyy').format(_rtrs[i].visitDate)
        : "Data N/A";

    return Card(
      margin: EdgeInsets.only(left: 5, top: 5, right: 8, bottom: 5),
      child: InkWell(
        onLongPress: () => _deleteVisit(context, i),
        onTap: () => _openRtr(context, i),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      _rtrs[i].stage.name != null
                          ? "Em processo: " + _rtrs[i].stage.name
                          : "Estagio da planta nao informado!",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              subtitle: Container(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(dateString),
              ),
            ),
            ButtonBarTheme(
              data: ButtonBarThemeData(buttonTextTheme: ButtonTextTheme.accent),
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        _openRtr(context, i);
                      },
                      child: Text("Detalhes")),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _openRtr(context, int i) {
    push(context, VisitTabsPage(true, visit: _rtrs[i]));
  }

  _deleteVisit(BuildContext context, int i) {
    alert(context, "Deseja mesmo excluir essa visita?", callback: () async {
      OdooResponse response = await RtrApi.deleteRtr(base, _rtrs[i].id);

      if (response != null) {
        alert(context, response.getResult());
      } else {
        RtrDao().delete(_rtrs[i].id);
        alert(context, "Excluido com sucesso!",
            hideCancelBtn: true, callback: () => pop(context));
      }
    });
  }
}
