import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/daos/crops_dao.dart';
import 'package:vorazrtr/data/daos/stage_dao.dart';
import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/models/stage.dart';
import 'package:vorazrtr/widgets/app_textfield.dart';

class VisitFormPage extends StatefulWidget {
  final Rtr rtr;
  final tDescription = TextEditingController();
  final tConclusion = TextEditingController();
  Crops cropsSelected;
  Stage stageSelected;
  DateTime visitDate;
  String _cropsName, _stageName;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  VisitFormPage({this.rtr});

  Rtr getRtr() {
    Rtr newRtr = Rtr();
    newRtr.description = tDescription.text;
    newRtr.finalConsiderations = tConclusion.text;
    newRtr.crops = cropsSelected;
    newRtr.stage = stageSelected;
    newRtr.visitDate = visitDate;

    return newRtr;
  }

  @override
  _VisitFormPageState createState() => _VisitFormPageState();
}

class _VisitFormPageState extends Base<VisitFormPage>
    with AutomaticKeepAliveClientMixin<VisitFormPage> {
  @override
  bool get wantKeepAlive => true;

  final List<DropdownMenuItem> cropsItemsDropdown = [];
  final List<DropdownMenuItem> stageItemsDropdown = [];

  final cropsDao = CropsDao();
  final stageDao = StageDao();

  @override
  void initState() {
    if (widget.rtr != null) {
      widget.tDescription.text = widget.rtr.description;
      widget.tConclusion.text = widget.rtr.finalConsiderations;

      widget.cropsSelected = widget.rtr.crops;
      widget._cropsName = widget.cropsSelected.name;
      widget.stageSelected = widget.rtr.stage;
      widget._stageName = widget.stageSelected.name;
    }

    super.initState();
    _getData();
  }

  @override
  void dispose() {
    widget.tDescription.dispose();
    widget.tConclusion.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        child: _form(),
      ),
    );
  }

  _form() {
    final format = DateFormat("dd/MM/yyyy");

    return Form(
      key: widget.formKey,
      child: ListView(
        children: <Widget>[
          SearchableDropdown.single(
            label: Text("Cultura: "),
            items: cropsItemsDropdown,
            value: widget._cropsName,
            hint: "Selecione a cultura",
            searchHint: "Cultura: ",
            onChanged: (value) {
              setState(() {
                widget._cropsName = value.name;
                widget.cropsSelected = value;
                print("crops: ${widget.cropsSelected.name}");
              });
            },
            isExpanded: true,
          ),
          SizedBox(height: 10),
          SearchableDropdown.single(
            label: Text("Estágio da planta: "),
            items: stageItemsDropdown,
            value: widget._stageName,
            hint: "Selecione o estágio da planta",
            searchHint: "Estágio da planta: ",
            onChanged: (value) {
              setState(() {
                widget._stageName = value.name;
                widget.stageSelected = value;
              });
            },
            isExpanded: true,
          ),
          SizedBox(height: 10),
          DateTimeField(
            maxLines: 1,
            decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.green, width: 0.0)),
                labelText: 'Data da visita',
                labelStyle: TextStyle(
                  fontSize: 20,
                  color: Colors.green,
                ),
                hintText: 'Data da visita'),
            format: format,
            onChanged: (date) {
              widget.visitDate = date;
            },
            initialValue:
                widget.rtr != null ? widget.rtr.visitDate : DateTime.now(),
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                  context: context,
                  firstDate: DateTime(1900),
                  initialDate: currentValue ?? DateTime.now(),
                  lastDate: DateTime(2100));
            },
          ),
          Divider(),
          AppTextfield("Descrição da visita",
              invisible: false,
              controller: widget.tDescription,
              inputType: TextInputType.multiline,
              maxLines: 5),
          AppTextfield("Conclusão",
              invisible: false,
              controller: widget.tConclusion,
              maxLines: 5,
              textCapitalization: TextCapitalization.words),
        ],
      ),
    );
  }

  void _getData() async {
    List<Crops> cropsList = await cropsDao.findAll();
    List<Stage> stageList = await stageDao.findAll();

    setState(() {
      //alimenta listas
      for (Crops crops in cropsList) {
        cropsItemsDropdown.add(DropdownMenuItem(
          child: Text(crops.name),
          value: crops,
        ));
      }
      for (Stage stage in stageList) {
        stageItemsDropdown.add(DropdownMenuItem(
          child: Text(stage.name),
          value: stage,
        ));
      }
    });
  }

//  Rtr setVisit() {
//    bool formOk = widget.formKey.currentState.validate();
//
//    if (!formOk) {
//      return null;
//    }
//
//    Rtr rtrObj = Rtr(
//        crops: widget.cropsSelected,
//        stage: widget.stageSelected,
//        visitDate: widget.visitDate,
//        description: widget.tDescription.text,
//        finalConsiderations: widget.tConclusion.text);
//    return rtrObj;
//  }
}
