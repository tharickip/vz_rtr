import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/pages/realtime_reports/rtr_form_page.dart';
import 'package:vorazrtr/pages/realtime_reports/rtr_page.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/easy_loading.dart';
import 'package:vorazrtr/utils/nav.dart';

class RtrsListview extends StatefulWidget {
  List<Rtr> rtrs;
  final Base base;

  RtrsListview(this.rtrs, {this.base});

  @override
  _RtrsListviewState createState() => _RtrsListviewState();
}

class _RtrsListviewState extends State<RtrsListview> {
  bool _connected = false;

  connection()async{
    bool connection;
    connection = await widget.base.isConnected();
   setState(() {
     _connected = connection;
   });
  }

  @override
  void initState() {
    super.initState();
    connection();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Relatórios em tempo-real'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: widget.rtrs.length,
          itemBuilder: (context, index) {
            return _rtrCard(context, index);
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () => push(context, RtrFormPage()),
      ),
    );
  }

  _rtrCard(BuildContext context, int i) {
    return Card(
      margin: EdgeInsets.only(left: 5, top: 5, right: 8, bottom: 5),
      child: InkWell(
        onTap: () => _openRtr(context, i),
        onLongPress: () => deleteRtr(context, i),
        child: Column(
          children: <Widget>[
            ListTile(
              leading: CircleAvatar(
                foregroundColor: Theme.of(context).primaryColor,
                backgroundColor: Colors.grey,
                backgroundImage: _connected ? (NetworkImage((widget.rtrs[i].farm != null &&
                        widget.rtrs[i].farm.imageUrl != null)
                      ? widget.rtrs[i].farm.imageUrl
                      : "https://cdn.iconscout.com/icon/free/png-512/avatar-380-456332.png"))
                    : null,
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      widget.rtrs[i].name,
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              subtitle: Container(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  widget.rtrs[i].farm.name +
                      ' / Visitas: ${widget.rtrs[i].countRtrChilds}',
                  style: TextStyle(color: Colors.grey, fontSize: 15.0),
                ),
              ),
            ),
            ButtonBarTheme(
              data: ButtonBarThemeData(buttonTextTheme: ButtonTextTheme.accent),
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        _openRtr(context, i);
                      },
                      child: Text("Detalhes")),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _openRtr(context, int i) {
    push(context, RtrPage(widget.rtrs[i]));
  }

  updateRtrs(int i) {
    setState(() {
      if (i != null) {
        widget.rtrs.removeAt(i);
      }
    });
  }

  void deleteRtr(BuildContext context, int i) {
    connection();
    if(_connected){
      alert(context, "Deseja mesmo excluir esse RTR?", callback: () async {
        //showLoading("Excluindo RTR...");
        OdooResponse response =
        await RtrApi.deleteRtr(widget.base, widget.rtrs[i].id);

        if (response != null) {
          alert(context, response.getResult());
        } else {
          RtrDao().delete(widget.rtrs[i].id);
          //showSuccess("Excluído!");
          updateRtrs(i);
        }
      });
    }else {
      /*alert(context, "Deseja mesmo excluir esse RTR?", callback: () async {
        showLoading("Excluindo RTR...");
        RtrDao().delete(widget.rtrs[i].id);
        showSuccess("Excluído!");
        updateRtrs(i);
      });*/
      alert(context, 'Só é possivel excluir caso tenha conexão!');
    }
  }
}
