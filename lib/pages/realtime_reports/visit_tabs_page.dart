import 'package:flutter/material.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/data/blocs/rtr_bloc.dart';
import 'package:vorazrtr/data/daos/multiimages_dao.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/models/multi_image.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/pages/realtime_reports/rtr_page.dart';
import 'package:vorazrtr/pages/realtime_reports/rtrs_page.dart';
import 'package:vorazrtr/pages/realtime_reports/visit_form_page.dart';
import 'package:vorazrtr/pages/realtime_reports/visit_page.dart';
import 'package:vorazrtr/pages/realtime_reports/visit_pics_form_page.dart';
import 'package:vorazrtr/pages/realtime_reports/visit_pics_page.dart';
import 'package:vorazrtr/utils/easy_loading.dart';
import 'package:vorazrtr/utils/nav.dart';

typedef VisitsValue = Rtr Function(Rtr);

class VisitTabsPage extends StatefulWidget {
  final bool viewOnly;
  final Rtr patternRtr;
  final Rtr visit;
  final VisitsValue callbackVisits;

  VisitTabsPage(this.viewOnly,
      {this.patternRtr, this.visit, this.callbackVisits});

  @override
  _VisitTabsPageState createState() => _VisitTabsPageState();
}

class _VisitTabsPageState extends Base<VisitTabsPage>
    with SingleTickerProviderStateMixin<VisitTabsPage> {
  TabController _tabController;

  VisitFormPage visitFormPage;
  VisitPicsFormPage visitPicsFormPage;
  bool _connected = false;

  _initTabs() async {
//    int index = await Base.getInt("tabIdx");
//
    _tabController = TabController(length: 2, vsync: this);
//    setState(() {
//      _tabController.index = index;
//    });
//
//    _tabController.addListener(() {
//      Base.setInt("tabIdx", _tabController.index);
//    });
  }

  @override
  void initState() {
    super.initState();
    connection();
    _initTabs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: widget.visit != null
            ? Text(widget.visit.description)
            : Text("Nova visita"),
        actions: <Widget>[
          IconButton(
            icon: widget.viewOnly ? Icon(Icons.edit) : Icon(Icons.save),
            onPressed: _onClickSaveEdit,
          ),
        ],
        bottom: _tabController == null
            ? null
            : TabBar(
                controller: _tabController,
                tabs: [
                  Tab(
                    text: "Informacoes",
                    icon: Icon(Icons.assignment),
                  ),
                  Tab(
                    text: "Fotos",
                    icon: Icon(Icons.add_a_photo),
                  ),
                ],
              ),
      ),
      body: _tabController == null
          ? null
          : TabBarView(
              controller: _tabController,
              children: widget.viewOnly
                  ? [
                      VisitPage(widget.visit),
                      VisitPicsPage(widget.visit),
                    ]
                  : [
                      visitFormPage = VisitFormPage(rtr: widget.visit),
                      visitPicsFormPage = VisitPicsFormPage(),
                    ],
            ),
    );
  }

  connection()async{
    bool connection;
    connection = await this.isConnected();
    setState(() {
      _connected = connection;
    });
  }

  Future<void> _onClickSaveEdit() async {
    if (widget.viewOnly) {
      //edit
      push(context, VisitTabsPage(false, visit: widget.visit));
    } else {
      bool formOk = visitFormPage.formKey.currentState.validate();

      if (!formOk) {
        return;
      }

      Rtr visit = visitFormPage.getRtr();
      visit.visitDate = visitFormPage.visitDate != null
          ? visitFormPage.visitDate
          : DateTime.now();
      visit.rtrId = widget.patternRtr.id;
      visit.type = "rtr";
      visit.reportName = widget.patternRtr.reportName;
      visit.farm = widget.patternRtr.farm;
      visit.multiImages = visitPicsFormPage.multiImages;

      if(_connected){
        //showLoading("Salvando visita...");
        int response = await RtrApi.saveRtr(this, visit.toMapToSend());
        if (response == 0) {
          //showError("Erro ao salvar visita!");
          pop(context);
        } else {
          //showSuccess("Visita salva com sucesso!");
          widget.callbackVisits(visit);
          pop(context);
        }
      }else{
        //showLoading("Salvando visita no Banco de Dados");
        RtrDao().save(visit);
        for(MultiImage _multiImage in visit.multiImages){
          _multiImage.visitId = visit.id;
          MultiImageDao().save(_multiImage);
        }
        //showSuccess("Visita salva com sucesso!");
        widget.callbackVisits(visit);
        pop(context);
      }
    }
  }
}
