import 'dart:collection';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/data/daos/crops_dao.dart';
import 'package:vorazrtr/data/daos/multiimages_dao.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/daos/stage_dao.dart';
import 'package:vorazrtr/data/models/annotation.dart';
import 'package:vorazrtr/data/models/choise.dart';
import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/data/models/multi_image.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/models/stage.dart';
import 'package:vorazrtr/pages/realtime_reports/rtr_annotation_page.dart';
import 'package:vorazrtr/pages/realtime_reports/rtr_pdf_page.dart';
import 'package:vorazrtr/pages/realtime_reports/visit_tabs_page.dart';
import 'package:vorazrtr/pages/realtime_reports/visits_listview.dart';
import 'package:vorazrtr/utils/my_text.dart';
import 'package:vorazrtr/utils/nav.dart';

import '../../utils/nav.dart';

class RtrPage extends StatefulWidget {
  Rtr rtr;

  RtrPage(this.rtr);

  @override
  _RtrPageState createState() => _RtrPageState();
}

const List<Choice> choices = const <Choice>[
  const Choice(
      option: 'anotacao', title: 'Nova anotação', icon: Icons.add_circle),
];

class _RtrPageState extends Base<RtrPage> {
  Base b;
  RtrApi rtrApi = RtrApi();
  VisitsListview listVisits = VisitsListview([]);
  List<Rtr> visits;

  Rtr get rtr => widget.rtr;

  List<Annotation> annotations = List();

  double heightAnnotations = 130;

  void _select(BuildContext context, Choice choice) async {
    if (choice.option == 'anotacao') {
      push(context, RtrAnnotationPage(this.rtr), replace: false)
          .then((dynamic id) async {
        this.rtr.messageIds.insert(0, id);
      });
    }
  }

  @override
  void initState() {
    super.initState();

    b = this;

    getVisits();
//    _getListAnnotations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 8),
            child: GestureDetector(
              child: Icon(Icons.picture_as_pdf),
              onTap: (){
                push(context, RtrPdfGen(widget.rtr, this.visits));
              },
            ),
          ),
        ],

        title: Text(rtr.name),
      ),
      body: _body(),
      floatingActionButton: FloatingActionButton.extended(
          icon: Icon(
            Icons.add,
            color: Colors.white,
          ),
          label: Text(
            "Visita",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => VisitTabsPage(
                          false,
                          patternRtr: rtr,
                          callbackVisits: (value) {
                            if (value != null) {
                              visits.add(value);
                              listVisits = VisitsListview(visits, base: b);
                            }
                            setState(() {

                            });
                          },
                        )));
          }),
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.all(16),
      child: _rtrDetails(),
    );
  }

  _rtrDetails() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Flexible(
          child: Column(
            children: <Widget>[
              CachedNetworkImage(height: 75, imageUrl: rtr.farm.imageUrl ?? ""),
              _textViews(),
              Divider(height: 25),
              listVisits != null ? _visits() : null,
//              Divider(
//                color: Colors.grey[500],
//                height: 20,
//              ),
//              _barAnnotations(),
//              Container(
//                  height: heightAnnotations,
//                  child: Card(child: RtrAnnotationListView(annotations))),
            ],
          ),
        ),
      ],
    );
  }

  _barAnnotations() {
    return InkWell(
      onTap: () {
        setState(() {
          heightAnnotations = heightAnnotations == 0 ? 130 : 0;
        });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Anotações"),
          IconButton(
              onPressed: () => print('pressed'),
              icon: Icon(heightAnnotations == 0 ? Icons.add : Icons.remove)),
        ],
      ),
    );
  }

  Future<void> _getListAnnotations() async {
    List<dynamic> annotationsStr =
        await rtrApi.readAnnotations(this, this.rtr.messageIds);

    for (LinkedHashMap<String, dynamic> annotationStr in annotationsStr) {
      Annotation annotation = Annotation();

      String body = makeUpBodyAnnotation(annotationStr["body"]);

      if (body.length > 0) {
        annotation.author = annotationStr["author_id"][1];
        annotation.annotation = body;

        setState(() {
          annotations.add(annotation);
        });
      }
    }
  }

  String makeUpBodyAnnotation(String body) {
    return body.replaceAll("<p>", "").replaceAll("</p>", "").trim();
  }

  _visits() {
    return Expanded(child: listVisits);
  }

  _textViews() {
    return Container(
      padding: EdgeInsets.only(top: 15, bottom: 10, left: 5, right: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Column(
              children: <Widget>[
                text(rtr.reportName, fontSize: 20, bold: true),
                text("Fazenda: ${rtr.farm.name}"),
                text("Campanha: ${rtr.campaign ?? ""}"),
                text("Estado: ${rtr.state != null ? rtr.state.name : "N/A"}"),
                text("Cidade: ${rtr.city != null ? rtr.city.name : "N/A"}"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void getVisits() async {
    visits = await RtrDao().findVisits(rtr.id ?? 0);

    for (Rtr visit in visits) {
      try {
        if (visit.crops.id != null) {
          Crops c = await CropsDao().findById(visit.crops.id);
          visit.crops = c;
        }
        if (visit.stage.id != null) {
          Stage s = await StageDao().findById(visit.stage.id);
          visit.stage = s;
        }
        /*if (visit.id != null) {
          List<MultiImage> images = await MultiImageDao().findMultiImages(visit.id);
          visit.multiImages = images;
        }*/
      } catch (e) {
        print(e);
      }
    }

    setState(() {
      listVisits = VisitsListview(visits, base: b);
    });
  }
}
