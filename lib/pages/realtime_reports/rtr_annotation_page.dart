import 'package:flutter/material.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/nav.dart';
import 'package:vorazrtr/widgets/app_button.dart';

class RtrAnnotationPage extends StatefulWidget {

  final Rtr rtr;
  RtrAnnotationPage(this.rtr);

  @override
  _RtrAnnotationPageState createState() => _RtrAnnotationPageState();
}

class _RtrAnnotationPageState extends Base<RtrAnnotationPage> {

  Rtr get _rtr => widget.rtr;

  RtrApi rtrApi = RtrApi();

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final textAnnotation = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Nova Anotação"),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            FocusScope.of(context).unfocus();
            Navigator.pop(context);
          }
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: _form(),
      ),
    );
  }

  _form() {
    return Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          TextFormField(
            autofocus: false,
            controller: textAnnotation,
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.green, width: 0.0)),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
              hintText: "Anotação",
              labelStyle: TextStyle(
                fontSize: 20,
                color: Colors.green,
              ),
            ),
            validator: (value) => value.isEmpty ? 'Campo obrigatório' : null,
            minLines: 9,
            maxLines: 15,
          ),
          Divider(),
          AppButton(
            "Gravar",
            onPressed: _sendAnnotation,
          )
        ],
      ),
    );
  }

  void _sendAnnotation() async {
    bool formOk = _formKey.currentState.validate();

    if (!formOk) {
      return;
    }

    FocusScope.of(context).unfocus();

    dynamic idAnnotation = await rtrApi.sendAnnotation(
      this, [_rtr.id], textAnnotation.text);

    if (idAnnotation != null) {
      alert(context, "Anotação registrada com sucesso!",
        hideCancelBtn: true, callback: () => pop(context, idAnnotation));
    } else {
      alert(context, "Falha ao realizar anotação!", hideCancelBtn: true);
    }
  }
}
