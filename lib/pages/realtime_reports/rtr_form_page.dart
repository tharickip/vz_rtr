import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/data/daos/campaign_dao.dart';
import 'package:vorazrtr/data/daos/city_dao.dart';
import 'package:vorazrtr/data/daos/partner_dao.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/daos/state_dao.dart';
import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/pages/realtime_reports/rtrs_page.dart';
import 'package:vorazrtr/utils/easy_loading.dart';
import 'package:vorazrtr/utils/nav.dart';
import 'package:vorazrtr/widgets/app_textfield.dart';

class RtrFormPage extends StatefulWidget {
  final Partner partner;
  String _partnerName, _campaignName, _stateName, _cityName;
  Partner partnerSelected = Partner();
  Campaign campaignSelected = Campaign();
  CountryState stateSelected = CountryState();
  City citySelected = City();

  RtrFormPage({this.partner});

  @override
  _RtrFormPageState createState() => _RtrFormPageState();
}

class _RtrFormPageState extends Base<RtrFormPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final tName = TextEditingController();
  final tFarm = TextEditingController();
  final tCampaign = TextEditingController();
  final _focusName = FocusNode();
  final List<DropdownMenuItem> partnerItemsDropdown = [];
  final List<DropdownMenuItem> campaignItemsDropdown = [];
  final List<DropdownMenuItem> stateItemsDropdown = [];
  List<DropdownMenuItem> cityItemsDropdown = [];
  final PartnerDao partnerDao = PartnerDao();
  final CampaignDao campaignDao = CampaignDao();
  final CityDao cityDao = CityDao();
  final StateDao stateDao = StateDao();

  bool _connected;

  connection()async{
    bool connection;
    connection = await this.isConnected();
    setState(() {
      _connected = connection;
    });
  }

  @override
  void initState() {
    connection();
    if (widget.partner != null) {
      widget.partnerSelected = widget.partner;
      widget._partnerName = widget.partner != null ? widget.partner.name : "";
      setState(() {});
    }
    _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Novo RTR"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.save),
              tooltip: "Salvar",
              onPressed: _onClickSave,
            ),
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: _form(),
        ),
      ),
    );
  }

  _form() {
    return Form(
      key: this._formKey,
      child: ListView(
        children: <Widget>[
          Container(
            child: widget.partnerSelected.imageUrl != null
                ? CachedNetworkImage(
                    width: 150,
                    height: 150,
                    imageUrl: widget.partnerSelected.imageUrl)
                : _img(),
          ),
          Divider(),
          AppTextfield(
            "Nome para o relatório",
            controller: tName,
            maxLines: 1,
            focusNode: _focusName,
            textCapitalization: TextCapitalization.sentences,
          ),
          Divider(),
          SearchableDropdown.single(
            label: Text("Fazenda:"),
            items: partnerItemsDropdown,
            value: widget._partnerName,
            hint: "Selecione a fazenda",
            searchHint: "Fazenda: ",
            onChanged: (value) async {
              Partner partner = await partnerDao.findByName(value);
              setState(() {
                widget._partnerName = value;
                widget.partnerSelected = partner;
              });
            },
            isExpanded: true,
          ),
          Divider(),
          SearchableDropdown.single(
            label: Text("Campanha:"),
            items: campaignItemsDropdown,
            value: widget._campaignName,
            hint: "Selecione a campanha",
            searchHint: "Campanha: ",
            onChanged: (value) {
              setState(() {
                widget._campaignName = value.name;
                widget.campaignSelected = value;
              });
            },
            isExpanded: true,
          ),
          Divider(),
          SearchableDropdown.single(
            label: Text("Estado:"),
            items: stateItemsDropdown,
            value: widget._stateName,
            hint: "Selecione o estado",
            searchHint: "Estado: ",
            onChanged: (value) {
              setState(() {
                widget._stateName = value.name;
                widget.stateSelected = value;
                _getCityData();
              });
            },
            isExpanded: true,
          ),
          Divider(),
          SearchableDropdown.single(
            label: Text("Cidade:"),
            items: cityItemsDropdown,
            value: widget._cityName,
            hint: "Selecione a cidade",
            searchHint: "Cidade: ",
            onChanged: (value) {
              setState(() {
                widget._cityName = value != null ? value.name : "";
                widget.citySelected = value;
              });
            },
            isExpanded: true,
          )
        ],
      ),
    );
  }

  Image _img() {
    return Image.asset(
      "assets/images/farmer.png",
      fit: BoxFit.contain,
      width: 150,
      height: 150,
    );
  }

  _getData() async {
    List<Partner> partnerDaoList = await partnerDao.findAll();
    List<Campaign> campaignDaoList = await campaignDao.findCampaigns();
    List<CountryState> stateDaoList = await stateDao.findAll();

    setState(() {
      //alimenta lista de clientes
      for (Partner p in partnerDaoList) {
        partnerItemsDropdown.add(DropdownMenuItem(
          child: Text(p.name),
          value: p.name,
        ));
      }
      //alimenta lista de campanhas
      for (Campaign c in campaignDaoList) {
        campaignItemsDropdown.add(DropdownMenuItem(
          child: Text(c.name),
          value: c,
        ));
      }
      //alimenta lista de estados
      for (CountryState state in stateDaoList) {
        stateItemsDropdown.add(DropdownMenuItem(
          child: Text(state.name),
          value: state,
        ));
      }
    });
  }

  _getCityData() async {
    List<City> cityDaoList =
        await cityDao.findCityByState(widget.stateSelected.id);
    cityItemsDropdown = [];
    //cidades
    for (City city in cityDaoList) {
      cityItemsDropdown.add(DropdownMenuItem(
        child: Text(city.name),
        value: city,
      ));
    }
  }

  _onClickSave() async {
    bool formOk = _formKey.currentState.validate();

    if (!formOk) {
      return;
    }

    //EasyLoading.show(status: 'Salvando RTR...');

    var rtr = Rtr(
      reportName: tName.text,
      farm: widget.partnerSelected,
      campaign: widget.campaignSelected,
      state: widget.stateSelected,
      type: "rtr",
      isRtr: "true",
      city: widget.citySelected,
    );


    if(_connected){
      int response = await RtrApi.saveRtr(this, rtr.toMap());

      if (response == 0) {
        //showError("Erro ao salvar visita!");
        pop(context);
      } else {
       // showSuccess("RTR salvo com sucesso!");
        pop(context);
        push(context, RtrsPage(), replace: true);
      }
    }else{
      rtr.name ='OFF ' + tName.text;
      print('Sem Conexão - RtrFormPage - Create RTR');
      RtrDao().save(rtr);
     // showSuccess("RTR salvo no Banco de Dados!");
      pop(context);
      push(context, RtrsPage(), replace: true);
    }
  }

//  Future<bool> _onBackPressed() async {
//    push(context, RtrsPage());
//  }
}
