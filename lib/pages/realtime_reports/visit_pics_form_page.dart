import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vorazrtr/data/models/multi_image.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/pages/realtime_reports/img_detail_page.dart';
import 'package:vorazrtr/utils/easy_loading.dart';
import 'package:vorazrtr/utils/nav.dart';

class VisitPicsFormPage extends StatefulWidget {
  final Rtr rtr;
  List<MultiImage> multiImages = [];

  VisitPicsFormPage({this.rtr});

  @override
  _VisitPicsFormPageState createState() => _VisitPicsFormPageState();
}

class _VisitPicsFormPageState extends State<VisitPicsFormPage>
    with AutomaticKeepAliveClientMixin<VisitPicsFormPage> {
  @override
  bool get wantKeepAlive => true;

  List<File> _files = [];

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      body: _defaultPic(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _openCamera(),
        child: Icon(
          Icons.photo_camera,
          color: Colors.white,
        ),
      ),
    );
  }

  _body() {
    return Container(
      margin: EdgeInsets.all(12),
      child: GridView.builder(
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: _files.length,
        itemBuilder: (BuildContext context, int index) {
          return _itemView(_files, index);
        },
      ),
    );
  }

  _defaultPic() {
    return Container(
      child: _files.length == 0
          ? Center(
              child: InkWell(
                child: Image.asset(
                  "assets/images/camera.png",
                  height: 150,
                ),
              ),
            )
          : _body(),
    );
  }

  _openCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    print(image);

    //showLoading("Processando foto...");
    List<int> Listimage = await testCompressFile(image);
    File fileFromInt = await File(image.path).writeAsBytes(Listimage);
    //add img to list
    String b64 = base64Encode(fileFromInt.readAsBytesSync());
    MultiImage multiImage = MultiImage();
    multiImage.image = b64;
    multiImage.title = "Título da imagem";
    widget.multiImages.add(multiImage);

    //dismiss();
    if (image != null) {
      setState(() {
        this._files.add(image);
      });
    }
  }

  _itemView(List<File> files, int index) {
    File f = files[index];
    int indexPlus = index + 1;

    return GestureDetector(
      onTap: () {
        push(context, ImgDetailPage(file: f));
      },
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          _img(f),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.all(12),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.black45,
                  borderRadius: BorderRadius.circular(6)),
              child: Text(
                "Imagem $indexPlus",
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _img(picture) {
    return Image.file(picture);
  }

  Future<List<int>> testCompressFile(File file) async {
    var result = await FlutterImageCompress.compressWithFile(
      file.absolute.path,
      minWidth: 2300,
      minHeight: 1500,
      quality: 64,
      format: CompressFormat.png,
    );
    return result;
  }
}
