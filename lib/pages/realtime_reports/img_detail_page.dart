import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';

class ImgDetailPage extends StatelessWidget {

  final File file;
  final Uint8List base64;

  ImgDetailPage({this.file, this.base64});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Imagem detalhada"),
      ),
      body: file != null ? Image.file(file) : Image.memory(base64),
    );
  }
}
