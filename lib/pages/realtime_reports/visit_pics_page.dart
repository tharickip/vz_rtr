import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/data/models/multi_image.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/pages/realtime_reports/img_detail_page.dart';
import 'package:vorazrtr/utils/nav.dart';

class VisitPicsPage extends StatefulWidget {
  Rtr rtr;

  VisitPicsPage(this.rtr);

  @override
  _VisitPicsPageState createState() => _VisitPicsPageState();
}

class _VisitPicsPageState extends Base<VisitPicsPage>
    with AutomaticKeepAliveClientMixin<VisitPicsPage> {
  @override
  bool get wantKeepAlive => true;
  RtrApi rtrApi = RtrApi();
  List<MultiImage> _multiImages = [];
  bool state = false;

  @override
  void initState() {
    super.initState();

    _getPictures();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
      body: _defaultPic(),
    );
  }

  _defaultPic() {
    return Container(
      child: _multiImages.length == 0
          ? Center(
              child: InkWell(
                child: Image.asset(
                  "assets/images/camera.png",
                  height: 150,
                ),
              ),
            )
          : _body(),
    );
  }

  _body() {
    return Container(
      child: GridView.builder(
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: _multiImages.length,
        itemBuilder: (BuildContext context, int index) {
          return _itemView(_multiImages, index);
        },
      ),
    );
  }

  _itemView(List<MultiImage> files, int index) {
    MultiImage image = files[index];
    Uint8List _base64 = Base64Codec().decode(image.image);

    return GestureDetector(
      onTap: () {
        push(context, ImgDetailPage(base64: _base64));
      },
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          _img(_base64),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.all(12),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.black45,
                  borderRadius: BorderRadius.circular(6)),
              child: Text(
                image.title,
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _getPictures() async {
    if (widget.rtr.multiImages.length > 0) {
      _multiImages = await rtrApi.getMultiImage(this, widget.rtr.multiImages);

      if (_multiImages.length > 0 && !state) {
        setState(() {
          state = true;
        });
      }
    }
  }

  _img(Uint8List base64) {
    return Image.memory(base64);
  }
}
