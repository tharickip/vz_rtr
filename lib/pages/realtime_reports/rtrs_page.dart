import 'dart:async';
import 'package:flutter/material.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/data/blocs/rtr_bloc.dart';
import 'package:vorazrtr/data/daos/campaign_dao.dart';
import 'package:vorazrtr/data/daos/city_dao.dart';
import 'package:vorazrtr/data/daos/multiimages_dao.dart';
import 'package:vorazrtr/data/daos/partner_dao.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/daos/state_dao.dart';
import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/data/models/multi_image.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/data/services/odoo_api.dart';
import 'package:vorazrtr/pages/realtime_reports/rtrs_listview.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/easy_loading.dart';
import 'package:vorazrtr/utils/event_bus.dart';
import 'package:vorazrtr/utils/nav.dart';

class RtrsPage extends StatefulWidget {
  @override
  _RtrsPageState createState() => _RtrsPageState();
}

class _RtrsPageState extends Base<RtrsPage>
    with AutomaticKeepAliveClientMixin<RtrsPage> {
  StreamSubscription<Event> subscription;

  final _bloc = RtrBloc();
  bool _loadingbd = false;
  bool _connected = false;

  @override
  bool get wantKeepAlive => true;

  Future<Odoo> odooInstance() async {
    Odoo odoo = await getOdooInstance();
    return odoo;
  }

  @override
  void initState() {
    super.initState();

    Base b = this;

    _bloc.fetchList(b);

    final bus = EventBus.get(context);
    subscription = bus.stream.listen((Event e) {
      _bloc.fetchList(b);
    });

    onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return alert(context, "Não foi possível buscar os RTRs!");
        }

        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        List<Rtr> rtrs = [];
        for (Rtr obj in snapshot.data) {
          if (obj.isRtr != null && obj.isRtr == 'true') {
            rtrs.add(obj);
          }
        }

        rtrs.sort((a, b) => b.name.compareTo(a.name));

        if (rtrs.length > 0 && rtrs[0].farm.name == null) {
          _getFullData(rtrs);
        }

        if(_loadingbd){
          return Center(
              child: CircularProgressIndicator(),
          );
        }

        return RefreshIndicator(
          onRefresh: onRefresh,
          child: RtrsListview(rtrs, base: this)
        );
      },
    );
  }

  connection()async{
    bool connection;
    connection = await this.isConnected();
    setState(() {
      _connected = connection;
    });
  }

  Future<void> onRefresh() async {
    connection();
    List<Rtr> _rtrBanco = await RtrDao().findAll();
    List<Rtr> _rtrServidor = await _bloc.fetchUpdate(this);

    bool _contain;
    bool _alterado = false;

    if(_connected){
      //Inserir no servidor os RTRs criados offline.
      for (Rtr rtb in _rtrBanco){
        for (Rtr rts in _rtrServidor){
          if(rtb.id == rts.id){
            _contain = true;
          }
        }
        if(!_contain){
          if(rtb.name != null){
            rtb.reportName = rtb.name.substring(4);
            rtb.name = '';
          }

          if(rtb.isRtr != 'true'){
            rtb.multiImages = await MultiImageDao().findMultiImages(rtb.id);
          }

          await RtrApi.saveRtr(this, rtb.toMap());
          _alterado = true;
        }
        _contain = false;
      }

      _rtrServidor = await _bloc.fetchUpdate(this);

      if(_alterado){
        RtrDao().deleteAll();
        for(Rtr rts in _rtrServidor){
          RtrDao().save(rts);
        }
      }

      return _rtrServidor;

      /*Analisar essa função V - Banco pode estar vazio e acabar excluindo tudo.
      //Remover no Servidor os RTRs deletados offline.
      for (Rtr rts in _rtrServidor){
        for (Rtr rtb in _rtrBanco){
          if(rts.id == rtb.id){
            _contain = true;
          }
        }
        if(!_contain){
          await RtrApi.deleteRtr(this, rts.id);
        }
        _contain = false;
      }*/

    }else{
      return _rtrBanco;
    }
  }

  @override
  void dispose() {
    super.dispose();

    _bloc.dispose();
    subscription.cancel();
  }

  _getFullData(List<Rtr> rtrs) async {
    _loadingbd = true;

    for (Rtr rtr in rtrs) {
      try {
        if (rtr.farm.name == null) {
          Partner p = await PartnerDao().findById(rtr.farm.id);
          rtr.farm = p;
        }
        if (rtr.campaign != null && rtr.campaign.name == null) {
          Campaign campaign = await CampaignDao().findById(rtr.campaign.id);
          rtr.campaign = campaign;
        }
        if (rtr.state != null && rtr.state.name == null) {
          CountryState state = await StateDao().findById(rtr.state.id);
          rtr.state = state;
        }
        if (rtr.city != null && rtr.city.name == null) {
          City city = await CityDao().findById(rtr.city.id);
          rtr.city = city;
        }
      } catch (e) {
        print(e);
      }
    }
    setState(() {
      _loadingbd = false;
    });
  }
}
