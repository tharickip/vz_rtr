import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vorazrtr/data/models/annotation.dart';

class RtrAnnotationListView extends StatefulWidget {

  final List<Annotation> _annotations;

  const RtrAnnotationListView(this._annotations);

  @override
  _RtrAnnotationListViewState createState() => _RtrAnnotationListViewState();
}

class _RtrAnnotationListViewState extends State<RtrAnnotationListView> {

  List<Annotation> get _annotations => widget._annotations;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.separated(
        itemCount: _annotations.length,
        separatorBuilder: (context, index) {
          return Divider();
        },
        itemBuilder: (context, index) {
          return Center(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(_annotations[index].author,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.grey[700],
                    )
                  ),
                  Flexible(
                    child: Text(_annotations[index].annotation,
                      style: TextStyle(
                        fontSize: 16
                      )
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
