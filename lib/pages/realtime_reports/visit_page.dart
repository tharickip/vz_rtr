import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/daos/rtr_dao.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/utils/my_text.dart';

class VisitPage extends StatefulWidget {
  Rtr rtr;

  VisitPage(this.rtr);

  @override
  _VisitPageState createState() => _VisitPageState();
}

class _VisitPageState extends Base<VisitPage>
    with AutomaticKeepAliveClientMixin<VisitPage> {
  @override
  bool get wantKeepAlive => true;

  Rtr get rtr => widget.rtr;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      padding: EdgeInsets.all(16),
      child: _body(),
    );
  }

  _body() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Flexible(
          child: Column(
            children: <Widget>[_textViews()],
          ),
        ),
      ],
    );
  }

  _textViews() {
    return Container(
      padding: EdgeInsets.all(6),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                text(
                    "Em processo: ${rtr.stage.name != null ? rtr.stage.name : "N/A"}",
                    fontSize: 20,
                    bold: true),
                text(
                    "Data da visita: ${rtr.visitDate != null ? DateFormat("dd/MM/yyyy").format(rtr.visitDate) : 'N/A'}",
                    fontSize: 20,
                    bold: true),
                Divider(),
                text(
                    "Cultura: ${rtr.crops.name != null ? rtr.crops.name : "N/A"}"),
                SizedBox(height: 10),
                text("Descrição da visita: ${rtr.description}", maxLines: 10),
                SizedBox(height: 10),
                text("Conclusão: ${rtr.finalConsiderations}", maxLines: 10),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
