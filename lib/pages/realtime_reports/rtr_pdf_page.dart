import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:printing/printing.dart';
import 'package:vorazrtr/data/models/multi_image.dart';
import '../../base.dart';
import '../../data/apis/rtr_api.dart';
import '../../data/models/rtr.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'dart:typed_data';

class RtrPdfGen extends StatefulWidget {
  Rtr rtr;
  List<Rtr> visits;

  RtrPdfGen(this.rtr, this.visits);

  @override
  _RtrPdfGenState createState() => _RtrPdfGenState();
}

class _RtrPdfGenState extends Base<RtrPdfGen> {
  RtrApi rtrApi = RtrApi();

  List<List<MultiImage>> _allImages = [];
  List<MultiImage> _multiImages = [];
  bool _loadingImages = true;
  int i = 0;

  @override
  void initState() {
    super.initState();
    _getPics();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PDF - ' + widget.rtr.name),
      ),
      body: !_loadingImages
        ? Container(
          child: PdfPreview(
            canChangePageFormat: false,
            maxPageWidth: 1000,
            onError: (context){
              return Container(
                child: Center(
                  child: Text('Montando PDF...'),
                ),
              );
            },
            build: generatePdf,
          ),
        )
      : Container(
        child: Center(
          child: Text('Montando PDF...'),
        ),
      )
    );
  }

  Future<Uint8List> generatePdf(PdfPageFormat pageFormat) async {
    final document = pw.Document();
    document.addPage(
      pw.MultiPage(
        maxPages: 20,
        pageFormat: pageFormat,
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        header: (pw.Context context){
          return pw.Container(
            child: pw.Column(
              children: [
                pw.Text(
                  widget.rtr.name,
                  style: pw.TextStyle(
                    color: PdfColors.black,
                    fontSize: 25,
                  ),
                ),
                pw.Divider(),
              ]
            ),
          );
        },
        build: (pw.Context context) => <pw.Widget>[
          pw.Column(
            children: [
              pw.Row(
                mainAxisAlignment: pw.MainAxisAlignment.start,
                children: [
                  pw.Text(
                    'Dados Gerais: ',
                    style: pw.TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ]
              ),
            ]
          ),
          pw.Row(
            mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
            children: [
              pw.Expanded(
                child: pw.Column(
                  crossAxisAlignment: pw.CrossAxisAlignment.start,
                  children: [
                    pw.Text(
                      'Cliente: ' ,
                    ),
                    pw.Text(
                      widget.rtr.farm != null
                        ? 'Fazenda: ' + widget.rtr.farm.name
                        : 'Fazenda: ' + 'N/A',
                    ),
                    pw.Text(
                      widget.rtr.city != null
                        ? 'Propiedade: ' + widget.rtr.city.name
                        : 'Propiedade: ' + 'N/A',
                    ),
                    pw.Text(
                      widget.rtr.crops != null
                        ? 'Safra: ' + widget.rtr.crops.name
                        : 'Safra: ' + 'N/A',
                    ),
                  ]
                ),
              ),
              pw.Expanded(
                child: pw.Column(
                  crossAxisAlignment: pw.CrossAxisAlignment.start,
                  children: [
                    pw.Text(
                      'CTV: ',
                    ),
                    pw.Text(
                      'Sup. Comercial: ',
                    ),
                    pw.Text(
                      'Data: ',
                    ),
                    pw.Text(
                      'Parceiro: ',
                    ),
                  ]
                ),
              ),
            ]
          ),
          pw.Divider(),
          pw.Wrap(
            children: List<pw.Widget>.generate(widget.visits.length,(int index){
              return _visitsPdfGen(context, index, document);
            }),
          ),
        ]
      ),
    );

    document.save();
    // Return the PDF file content
    return document.save();
  }

  _visitsPdfGen(pw.Context context, int index, pw.Document document){
    return pw.Wrap(
      children: [
        pw.Wrap(
          children: [
            pw.Paragraph(
              text: 'Informações da Visita: ' + widget.visits[index].description,
            ),
          ]
        ),
        pw.Wrap(
          children: [
            pw.Flex(
              direction: pw.Axis.horizontal,
              mainAxisAlignment: pw.MainAxisAlignment.center,
              children: [
                pw.ListView.builder(
                  direction: pw.Axis.vertical,
                  spacing: 10,
                  itemCount: (widget.visits[index].multiImages.length / 4).ceil(),
                  itemBuilder: (context, indexColumn) {
                    return pw.ListView.builder(
                      direction: pw.Axis.horizontal,
                      spacing: 20,
                      itemCount: widget.visits[index].multiImages.length - ((indexColumn + 1) * 4) >= 0
                        ? 4
                        : ( indexColumn > 0 ?  widget.visits[index].multiImages.length - ((indexColumn) * 4) : widget.visits[index].multiImages.length),
                      itemBuilder: (context, indexIm){
                        return widget.visits[index].multiImages.length - ((indexColumn + 1) * 4) >= 0
                        ? _visitsImages(context, index ,indexIm + (indexColumn * 4), document)
                        : (indexColumn > 0 ?  _visitsImages(context, index, ((indexColumn) * 4), document)
                            : _visitsImages(context, index, indexIm, document));
                      },
                    );
                  },
                )
              ]
            ),
          ]
        ),
        pw.Wrap(
          children: [
            pw.Paragraph(
              text: 'Conclusão: ' + widget.visits[index].finalConsiderations,
            ),

            pw.Divider(),

          ]
        ),

      ],
    );
  }

  _getPics() async {
    for(int index = 0; index < widget.visits.length; index++){
      if(widget.visits[index].multiImages.length > 0){
        _multiImages = await rtrApi.getMultiImage(this, widget.visits[index].multiImages);
        _allImages.add(_multiImages);
      }
      else{
        _allImages.add([]);
      }
    }
    setState(() {
      _loadingImages = false;
    });
  }

  _visitsImages(pw.Context context, int index ,int indexIm, pw.Document document){
    if(_allImages[index].length > 0 ){

      MultiImage image = _allImages[index][indexIm];
      Uint8List _base64 = Base64Codec().decode(image.image);

      PdfImage pdfIm = PdfImage.file(document.document, bytes: _base64);

      return pw.Wrap(
        children: [
          pw.Image(
            pdfIm,
            height: 150,//_allImages[index].length,
            width:  100,//allImages[index].lenght,
          ),
        ]
      );
    }
  }
}