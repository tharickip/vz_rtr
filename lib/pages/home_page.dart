import 'package:flare_dart/math/mat2d.dart';
import 'package:flare_flutter/flare.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:vorazrtr/drawer_list.dart';
import 'package:vorazrtr/pages/realtime_reports/rtrs_page.dart';
import 'package:vorazrtr/utils/nav.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with FlareController {
  double _rockAmount = 0.5;

  double _speed = 3.0;

  double _rockTime = 1.0;

  ActorAnimation _rock;

  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      push(context, RtrsPage());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("Bem-vindo ao Voraz RTR!")),
      drawer: DrawerList(),
      body: Center(
        child: Container(
            child:
                FlareActor("assets/animations/vz_logo.flr", animation: "go")),
      ),
//      body: Stack(
//        children: [
//          Positioned.fill(
//            child: FlareActor("assets/animations/Penguin.flr",
//                alignment: Alignment.center,
//                isPaused: false,
//                fit: BoxFit.cover,
//                animation: "walk",
//                controller: this),
//          )
//        ],
//      ),
    );
  }

  @override
  bool advance(FlutterActorArtboard artboard, double elapsed) {
    _rockTime += elapsed * _speed;
    _rock.apply(_rockTime % _rock.duration, artboard, _rockAmount);
    return true;
  }

  @override
  void initialize(FlutterActorArtboard artboard) {
    _rock = artboard.getAnimation("music_walk");
  }

  @override
  void setViewTransform(Mat2D viewTransform) {
    // TODO: implement setViewTransform
  }
}
