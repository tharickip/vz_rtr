import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:vorazrtr/splash_page.dart';
import 'package:vorazrtr/utils/event_bus.dart';
import 'package:vorazrtr/utils/strings.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<EventBus>(
        create: (context) => EventBus(),
        dispose: (context, bus) => bus.dispose(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: Strings.app_title,
        theme: ThemeData(
          primarySwatch: Colors.green,
          primaryColor: Colors.green,
          primaryColorDark: Colors.orange,
          brightness: Brightness.light,
          buttonTheme: ButtonThemeData(
            buttonColor: Colors.green,
            textTheme: ButtonTextTheme.primary,
          )
        ),
        home: FlutterEasyLoading(
          child: SplashPage(),
        ),
      ),
    );
  }
}
