import 'package:flutter/material.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/user.dart';
import 'package:vorazrtr/pages/login_page.dart';
import 'package:vorazrtr/pages/partners/partners_page.dart';
import 'package:vorazrtr/pages/realtime_reports/rtrs_page.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/nav.dart';

class DrawerList extends StatefulWidget {
  @override
  _DrawerListState createState() => _DrawerListState();
}

class _DrawerListState extends Base<DrawerList> {
  User user = new User();
  String session;
  String imageUrl;
  bool _connected = false;

  connection() async{
    _connected = await this.isConnected();
  }

  @override
  void initState() {
    super.initState();
    connection();

    getOdooInstance().then((odoo) {
      _checkFirstTime();
    });
  }

  _checkFirstTime() {
    if (getUser() != null) {
      setState(() {
        user = getUser();
        session = getSession();
        String sessionSplit = session.split(",")[0].split(";")[0];
        imageUrl = getURL() +
            "/web/image?model=res.users&field=image_medium&" +
            sessionSplit +
            "&id=" +
            getUID().toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName:
                  Text(user.result != null ? user.result.name : "Nome"),
              accountEmail:
                  Text(user.result != null ? user.result.username : "E-mail"),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.white,
                backgroundImage: _connected ? NetworkImage(imageUrl ?? "") : null,
              ),
            ),
            ListTile(
              leading: Icon(Icons.art_track),
              title: Text("RTR"),
              subtitle: Text("Seus relatórios em tempo real"),
              onTap: () {
                push(context, RtrsPage());
              },
            ),
            ListTile(
              leading: Icon(Icons.people),
              title: Text("Fazendas"),
              subtitle: Text("Clientes cadastrados"),
              onTap: () {
                push(context, PartnersPage());
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Logout"),
              trailing: Icon(Icons.arrow_forward),
              onTap: () {
                alert(context, "Deseja mesmo sair?",
                    callback: () => clearPrefs(LoginPage()));
              },
            )
          ],
        ),
      ),
    );
  }
}
