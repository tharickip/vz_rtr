CREATE TABLE state(id INTEGER PRIMARY KEY, name TEXT, code TEXT, country_id INTEGER);
CREATE TABLE city(id INTEGER PRIMARY KEY, name TEXT, state_id INTEGER);
CREATE TABLE crops(id INTEGER PRIMARY KEY, name TEXT, active TEXT);
CREATE TABLE stage(id INTEGER PRIMARY KEY, name TEXT);
CREATE TABLE campaign(id INTEGER PRIMARY KEY, name TEXT);
CREATE TABLE partner(id INTEGER PRIMARY KEY, name TEXT, email TEXT, phone TEXT, address TEXT,
imageUrl TEXT);
CREATE TABLE multi_images(id INTEGER PRIMARY KEY, title TEXT, image TEXT, visit_id INTEGER);
CREATE TABLE rtr(id INTEGER PRIMARY KEY, report_name TEXT, name TEXT, campaign_id INTEGER,
partner_id INTEGER, state_id INTEGER, city_id INTEGER, type TEXT, is_rtr TEXT, user_id INTEGER,
visit_date TEXT, date_deadline TEXT, count_rtr_childs INTEGER, crops_id INTEGER,
plant_stage_id INTEGER, description TEXT, final_considerations TEXT, rtr_id INTEGER,
multi_images INTEGER,
FOREIGN KEY(rtr_id) REFERENCES rtr(id), FOREIGN KEY(multi_images) REFERENCES multi_images(id));
